# Todo API
##### _Version: 1.0.0_
##### _Author: Jack Farrow_

## The World's Finest Todo App

**Todo API** is a **Spring Boot** application that leverages **JPA** and **Hibernate** to interface with a **PostgreSQL** database instance. 
## API Operations

## `Todo`

| Operation          | Method   | Endpoint                                      | Body                                                         | Purpose                                                | Returns                          |
|--------------------|----------|-----------------------------------------------|--------------------------------------------------------------|--------------------------------------------------------|----------------------------------|
| `GetTodosByUserId` | `GET`    | `/api/v1/todos?user={user_id}`                | null                                                         | Fetch all saved todos for user identified by `user_id` | `List<Todo>` \| {}               |
| `GetTodoByTodoId`  | `GET`    | `/api/v1/todos?user={user_id}&todo={todo_id}` | null                                                         | Fetch saved todo identified by `todo_id`               | `Todo`: the found `Todo` \| null |
| `CreateTodo`       | `POST`   | `/api/v1/todos/create`                        | `{"title": string, "description": string}`                   | Create new `Todo`                                      | `Todo`: the created `Todo`       |
| `UpdateTodo`       | `PUT`    | `/api/v1/todos/update?todo={todo_id}`         | `{"title": string, "description": string, "status": string}` | Update todo title, description, and/or status          | `Todo`: the updated `Todo`       |
| `DeleteTodoById`   | `DELETE` | `/api/v1/todos/delete?id={todo_id}`           | null                                                         | Delete todo identified by `todo_id`                    | `Todo`: the deleted `Todo`       |

## `User`

| Operation       | Method | Endpoint             | Body                                                                     | Purpose               | Returns                                     |
|-----------------|--------|----------------------|--------------------------------------------------------------------------|-----------------------|---------------------------------------------|
| `ResetPassword` | `PUT`  | `api/v1/user/update` | `{"username": string, "currentPassword": string, "newPassword": string}` | Reset user's password | `boolean`: true if success, false otherwise |

## `Auth`

| Operation | Method | Endpoint         | Body                                        | Purpose                                | Returns                                     |
|-----------|--------|------------------|---------------------------------------------|----------------------------------------|---------------------------------------------|
| `Signup`  | `POST` | `/api/v1/signup` | `{"username": string, "password": string }` | Register a new user to the application | `User`: the registered user                 |
| `Login`   | `POST` | `/api/v1/login`  | `{"username": string, "password": string}`  | Authenticate a user to the application | `JWT`: `{"user_id": string, "iat": number}` |
| `Logout`  | `POST` | `/api/v1/logout` | null                                        | Revoke the user's JWT                  | `boolean`: true if success, false otherwise |

## Data Models

### User

| field    | data type |
|----------|-----------|
| user_id  | UUID      |
| username | String    |
| password | String    |

### Todo

| field               | data type |
|---------------------|-----------|
| todo_id             | UUID      |
| title               | String    |
| description         | String    |
| created_date_time   | Timestamp |
| status              | String    |
| is_complete         | boolean   |
| completed_date_time | Timestamp |
| user_id             | UUID      |
