package com.todoapi.todo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;

class TestTodoService {

    @Mock
    private TodoRepository todoRepository;

    private TodoService underTest;
    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
       underTest = new TodoService(todoRepository);
    }

    @Test
    public void itShouldGetTodoById() {
        // Given
//        UUID todoId = UUID.randomUUID();
//        UUID userId = UUID.randomUUID();
//        String title = "Test Todo #1";
//        String description = "A test todo";
//        LocalDateTime createDate = LocalDateTime.of(2023, 1, 1, 10, 10);
//        TodoStatus status = TodoStatus.NOT_STARTED;
//        boolean isComplete = false;
//        LocalDateTime completedDate = LocalDateTime.now();
//
//        Todo todo = new Todo(
//                todoId,
//                userId,
//                title,
//                description,
//                createDate,
//                status,
//                isComplete,
//                completedDate
//                );
//        // When
//        given(todoRepository.findById(todoId)).willReturn(Optional.of(todo));
//        Optional<Todo> optionalTodo = underTest.getTodoById(todoId.toString());
//        // Then
//        assertThat(optionalTodo.isPresent());
//        assertThat(optionalTodo.get()).isEqualToComparingFieldByField(todo);

    }
}