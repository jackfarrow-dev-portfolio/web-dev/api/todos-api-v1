package com.todoapi.todo;

import com.todoapi.exception.BadRequestException;
import com.todoapi.exception.ServerException;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
public class TodoService {

    private final TodoRepository todoRepository;
    private static final Logger log = LoggerFactory.getLogger(TodoService.class);


    @Autowired
    public TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    public List<Todo> getTodosByUserId(String userId) {
        return this.todoRepository.getTodosByUserId(UUID.fromString(userId));
    }

    public Optional<Todo> getTodoByTodoId(String userId, String todoId) {
        Optional<Todo> foundTodo = Optional.empty();
        try {
            UUID userIdAsUUID = UUID.fromString(userId);
            UUID todoIdAsUUID = UUID.fromString(todoId);
            foundTodo = this.todoRepository.getTodoByUserIdAndTodoId(userIdAsUUID, todoIdAsUUID);
            if (foundTodo.isEmpty()) {
                log.info("Failed to fetch todo with id " + todoId + "; record not found");
                throw new BadRequestException("Todo not found");
            }
        } catch(IllegalArgumentException e) {
            log.info("Failed to fetch todo with id " + todoId + ";" + e.getMessage());
            throw new BadRequestException("Malformed user or todo ID");
        }
        return foundTodo;
    }

    public Todo createTodo(TodoRequest request) {
        Todo todo = new Todo(
                request.getTitle(),
                request.getDescription(),
                UUID.fromString(request.getUserId()),
                LocalDateTime.now(),
                request.parseStatus(),
                request.isComplete()
        );
        try {
            this.todoRepository.save(todo);
            return todo;
        } catch(Exception e) {
            log.info("Failed to save record '" + request.getTitle() + "': " + e.getMessage());
           throw new ServerException("Failed to save record '" + request.getTitle() + "' to " +
                   "database");
        }
    }

    public Todo updateTodo(TodoRequest request) {
        Optional<Todo> optional =
                this.todoRepository.findById(UUID.fromString(request.getTodoId()));
        if (optional.isPresent()) {
            Todo foundTodo = optional.get();
            foundTodo.setTitle(request.getTitle());
            foundTodo.setDescription(request.getDescription());
            foundTodo.setStatus(request.parseStatus());
            foundTodo.setComplete(request.isComplete());

            try {
                this.todoRepository.save(foundTodo);
                return foundTodo;
            } catch(Exception e) {
                log.info("Failed to update todo identified by " + request.getTodoId() + ": " + e.getMessage());
                throw new ServerException("Failed to update todo identified by " + request.getTodoId());
            }
        }
        else {
            log.info("Failed to fetch todo with id " + request.getTodoId() + "; record not found");
            throw new BadRequestException("Todo not found");
        }
    }

    public boolean deleteTodoById(String todoId) {
        try {
            this.todoRepository.deleteById(UUID.fromString(todoId));
        } catch(Exception e) {
            log.info("Failed to fetch todo with id " + todoId + "; record not found");
            throw new BadRequestException("Todo not found");
        }
        return true;
    }
}
