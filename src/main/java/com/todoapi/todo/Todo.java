package com.todoapi.todo;

import jakarta.persistence.*;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

@Entity(name="Todo")
@Table(name = "todos")
public class Todo {
    @Id
    @Column(name = "todo_id")
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID todoId;

    @Column(name = "user_id")
    private UUID userId;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "created_date_time")
    private LocalDateTime createdDateTime;

    @Column(name = "status")
    @Enumerated(value = EnumType.STRING)
    private TodoStatus status;

    @Column(name = "is_complete")
    private boolean isComplete;

    @Column(name = "completed_date_time")
    private LocalDateTime completedDateTime;

    public Todo() {
    }

    public Todo(
            String title,
            String description,
            UUID userId,
            LocalDateTime createdDateTime,
            TodoStatus status,
            boolean isComplete
    ) {
        this.title = title;
        this.description = description;
        this.userId = userId;
        this.createdDateTime = createdDateTime;
        this.status = status;
        this.isComplete = isComplete;
    }

    public Todo(
            UUID todoId,
            UUID userId,
            String title,
            String description,
            LocalDateTime createdDateTime,
            TodoStatus status,
            boolean isComplete,
            LocalDateTime completedDateTime
    ) {
        this.todoId = todoId;
        this.userId = userId;
        this.title = title;
        this.description = description;
        this.createdDateTime = createdDateTime;
        this.status = status;
        this.isComplete = isComplete;
        this.completedDateTime = completedDateTime;
    }
    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public LocalDateTime getCreatedDateTime() {
        return createdDateTime;
    }

    public TodoStatus getStatus() {
        return status;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public LocalDateTime getCompletedDateTime() {
        return completedDateTime;
    }

    public UUID getTodoId() {
        return todoId;
    }

    public void setTodoId(UUID todoId) {
        this.todoId = todoId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCreatedDateTime(LocalDateTime createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public void setStatus(TodoStatus status) {
        this.status = status;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }

    public void setCompletedDateTime(LocalDateTime completedDateTime) {
        this.completedDateTime = completedDateTime;
    }


    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Todo todo = (Todo) o;
        return isComplete == todo.isComplete && Objects.equals(todoId, todo.todoId) && Objects.equals(userId, todo.userId) && Objects.equals(title, todo.title) && Objects.equals(description, todo.description) && Objects.equals(createdDateTime, todo.createdDateTime) && status == todo.status && Objects.equals(completedDateTime, todo.completedDateTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(todoId, userId, title, description, createdDateTime, status, isComplete, completedDateTime);
    }

    @Override
    public String toString() {
        return "Todo{" +
                "userId=" + userId +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", createdDateTime=" + createdDateTime +
                ", status=" + status +
                ", isComplete=" + isComplete +
                ", completedDateTime=" + completedDateTime +
                '}';
    }
}
