package com.todoapi.todo;

public enum TodoStatus {
    NOT_STARTED,
    IN_PROGRESS,
    COMPLETE
}
