package com.todoapi.todo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface TodoRepository extends CrudRepository<Todo, UUID> {

    @Query(value = "SELECT t FROM Todo t WHERE t.userId = :userId")
    List<Todo> getTodosByUserId(@Param("userId") UUID userId);

    @Query(value = "SELECT t FROM Todo t WHERE t.userId = :userId AND t.todoId = :todoId")
    Optional<Todo> getTodoByUserIdAndTodoId(@Param("userId") UUID userId,
                                            @Param("todoId") UUID todoId);
}
