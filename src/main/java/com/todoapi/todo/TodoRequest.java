package com.todoapi.todo;

public class TodoRequest {
    private String todoId;
    private String userId;
    private String title;
    private String description;
    private String status;
    private boolean isComplete;

    public TodoRequest() {
    }

    public TodoRequest(
            String todoId,
            String userId,
            String title,
            String description,
            String status,
            boolean isComplete
    ) {
        this.todoId = todoId;
        this.userId = userId;
        this.title = title;
        this.description = description;
        this.status = status;
        this.isComplete = isComplete;
    }

    public TodoStatus parseStatus() {
        if (this.status.equals("In Progress")) {
            return TodoStatus.IN_PROGRESS;
        } else if (this.status.equals("Complete")) {
            return TodoStatus.COMPLETE;
        }
        return TodoStatus.NOT_STARTED;
    }

    public String getTodoId() {
        return todoId;
    }

    public void setTodoId(String todoId) {
        this.todoId = todoId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }
}
