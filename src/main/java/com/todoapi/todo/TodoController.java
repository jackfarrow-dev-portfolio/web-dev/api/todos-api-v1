package com.todoapi.todo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/v1/todos")
public class TodoController {

    private final TodoService todoService;

    @Autowired
    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }
    
    @GetMapping("get/all")
    public List<Todo> getTodosByUserId(@RequestParam("user") String userId) {
        return this.todoService.getTodosByUserId(userId);
    }

    @GetMapping("get")
    public Optional<Todo> getTodoByUserIdAndTodoId(@RequestParam("user") String userId,
                                                   @RequestParam("todo") String todoId) {
        return this.todoService.getTodoByTodoId(userId, todoId);
    }

    @PostMapping("create")
    public Todo createTodo(@RequestBody TodoRequest request) {
        return this.todoService.createTodo(request);
    }

    @DeleteMapping("delete")
    public boolean deleteTodByTodoId(@RequestParam("todo") String todoId) {
        return this.todoService.deleteTodoById(todoId);
    }
}
