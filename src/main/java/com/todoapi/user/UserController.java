package com.todoapi.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("api/v1/users")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }
    @PutMapping("update")
    public UserResponse resetPassword(@RequestBody User user) {
        User updatedUser = this.userService.resetPassword(user);
        return new UserResponse(updatedUser.getUsername(), updatedUser.getUserId().toString());
    }
}
