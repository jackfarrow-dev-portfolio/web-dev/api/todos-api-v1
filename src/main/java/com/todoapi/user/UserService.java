package com.todoapi.user;

import com.todoapi.exception.BadRequestException;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
@Slf4j
public class UserService {

    private final UserRepository userRepository;
    private static final Logger log = LoggerFactory.getLogger(UserService.class);

    @Autowired
    public UserService (UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User resetPassword(User user) {
        Optional<User> optional = this.userRepository.findByUsername(user.getUsername());
        if (optional.isPresent()) {
            try {
                User foundUser = optional.get();
                foundUser.setPassword(user.getPassword());
                this.userRepository.save(foundUser);
                return foundUser;
            } catch (Exception e) {
                log.info(e.getClass().toString());
                log.info("Failed to update user " + user.getUsername() + ": " + e.getMessage());
            }
        } else {
            log.info("Failed to update user " + user.getUsername() + ": not found");
            throw new BadRequestException("User identified by " + user.getUsername() + " not " +
                    "found");
        }
        return null;
    }
}
