package com.todoapi.exception;

import org.springframework.http.HttpStatus;

import java.time.ZonedDateTime;

public class ApiException {
    private final String message;
    private final Throwable cause;
    private final HttpStatus status;
    private final ZonedDateTime dateTime;

    public ApiException(
            String message,
            Throwable cause,
            HttpStatus status,
            ZonedDateTime dateTime
    ) {
        this.message = message;
        this.cause = cause;
        this.status = status;
        this.dateTime = dateTime;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getCause() {
        return cause;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public ZonedDateTime getDateTime() {
        return dateTime;
    }

    @Override
    public String toString() {
        return "ApiException{" +
                "message='" + message + '\'' +
                ", cause=" + cause +
                ", status=" + status +
                ", dateTime=" + dateTime +
                '}';
    }
}
