CREATE TABLE IF NOT EXISTS todos (
    todo_id UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    user_id UUID NOT NULL,
    title VARCHAR(125) NOT NULL,
    description VARCHAR(250) NOT NULL,
    created_date_time TIMESTAMP DEFAULT NOW() NOT NULL,
    status VARCHAR(25) NOT NULL,
    is_complete BOOLEAN NOT NULL,
    completed_date_time TIMESTAMP,
    CONSTRAINT todos_user_id_fk
    FOREIGN KEY user_id REFERENCES users(user_id)
    ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS users (
    user_id UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    username VARCHAR(50) NOT NULL,
    password VARCHAR(50) NOT NULL
);