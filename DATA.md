## Data Models

### Todo

| Attribute             | Type                                               |
| --------------------- | -------------------------------------------------- |
| `todo_id`             | UUID                                               |
| `title`               | String                                             |
| `description`         | String                                             |
| `created_date_time`   | Timestamp                                          |
| `status`              | String {"NOT_STARTED", "IN_PROGRESS", "COMPLETED"} |
| `is_complete`         | boolean                                            |
| `completed_date_time` | Timestamp                                          |

### User

| Attribute  | Type   |
| ---------- | ------ |
| `user_id`  | UUID   |
| `username` | String |
| `password` | String |

### UserTodo

| Attribute | Type |
| --------- | ---- |
| `user_id` | UUID |
| `todo_id` | UUID |
